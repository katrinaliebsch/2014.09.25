import Tkinter as tk
import sys

import numpy as numpy
import matplotlib
matplotlib.use('TkAgg')

from numpy import sin, pi, cos

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import matplotlib.animation as animation

import pylab as pyl
import scipy.integrate as integrate


# interface class defines the GUI that allows the user to change pendulum settings
# the interface class also displays the animation and the user selected graph
class Interface(tk.Frame):
	def __init__(self, master=None):
		tk.Frame.__init__(self, master)
		self.grid()

		#creates the figure in the interface that will contain the animation and graph
		self.fig = plt.figure(1)
		self.fig.set_size_inches(15,7.5)

		self.create_interface()

	def create_interface(self):
		# title label
		self.label_title = tk.Label(self, text="Planar Pendulum Simulation").grid(column=0, row=0, columnspan=2)

		# mass slider
		self.label_mass = tk.Label(self, text="Mass (kg)   ").grid(column=0, row=2)
		self.slider_mass = tk.Scale(self, from_=0.00, to=100.00, resolution=1.0, orient="horizontal")
		self.slider_mass.grid(column=1, row=2)
		self.slider_mass.set(50.00)

		# length slider
		self.label_length = tk.Label(self, text="Length (m)   ").grid(column=0, row=3)
		self.slider_length = tk.Scale(self, from_=0.00, to=10.00, resolution=1.0, orient="horizontal")
		self.slider_length.grid(column=1, row=3)
		self.slider_length.set(5.00)

		# friction coefficient input
		self.label_friction = tk.Label(self, text="Friction Coefficient (Nm / (rad/s))   ").grid(column=0, row=4)
		self.slider_friction = tk.Scale(self, from_=0.00, to=1000.00, resolution=0.1, orient="horizontal")
		self.slider_friction.grid(column=1, row=4)
		self.slider_friction.set(10.00)

		# initial angle input
		self.label_iangle = tk.Label(self, text="Initial Angle (Deg)   ").grid(column=0, row=5)
		self.iangle = tk.StringVar()
		initial_angle = tk.Entry(self, textvariable=self.iangle).grid(column=1, row=5)
		self.iangle.set("25.00")

		# final time input
		self.label_time_f = tk.Label(self, text="Final Time (s)   ").grid(column=0, row=6)
		self.time_f = tk.StringVar()
		entry_time_f = tk.Entry(self, textvariable=self.time_f).grid(column=1, row=6)
		self.time_f.set("10.00")

		# variable selector for the graph
		self.label_var_selec = tk.Label(self, text="Variable to Display   ").grid(column=0, row=7)
		self.var_selec = tk.StringVar()
		self.var_selec.set("Position (Angular)")
		option_var_selec = tk.OptionMenu(self, self.var_selec, "Position (Angular)", "Velocity (Angular)", "Position 2 (Angular)", "Velocity 2 (Angular)").grid(column=1, row=7)

		# run button
		self.button_run = tk.Button(self, text="Run")
		if simulation_toggle == 1:
			self.button_run['command'] = self.run1
		elif simulation_toggle == 2:
			self.button_run['command'] = self.run2
		self.button_run.grid(column=0, row=8)

		# quit button
		self.button_quit = tk.Button(self, text="Quit", command=self.quit).grid(column=1, row=8)

		# animation and graphing interface
		self.canvas = FigureCanvasTkAgg(self.fig, master=self)
		self.canvas.get_tk_widget().grid(column=3, row=2, columnspan=5, rowspan=7)
		self.canvas.show()

	# performs the simulation and graphing for the one-link pendulum
	def run1(self):
		self.fig.clear()

		# set constant variables
		gravity = 9.81
		delta_t = 0.1

		# obtain user defined variables from the GUI
		initial_angle = float(self.iangle.get())
		length = self.slider_length.get()
		mass = self.slider_mass.get()
		time_f = float(self.time_f.get())
		friction_coeff = self.slider_friction.get()

		# calculate inertia
		inertia = mass * length**2 / 3.0

		# set the initial conditions
		state = numpy.array([initial_angle, 0.0])*pi/180.0

		# function calculates the first and second derivates for odeint() to use
		def calc_derivative(state, time):
			dtheta_dt = numpy.zeros_like(state)
			dtheta_dt[0] = state[1]
			dtheta_dt[1] = -sin(state[0])*mass*gravity*length/2.0/inertia - state[1]*friction_coeff/inertia
			return dtheta_dt

		# creates time array that odeint() uses to integrate
		time = numpy.arange(0.0, time_f, delta_t)

		# use numerical integration to solve pendulum state equations
		theta = integrate.odeint(calc_derivative, state, time)

		# create arrays with coordinates of the link over time
		self.xx = length*sin(theta[:,0])
		self.yy = -length*cos(theta[:,0])

		# create the subplot for the animation
		ax = self.fig.add_subplot(121, autoscale_on=False, xlim=(-10,10), ylim=(-10,10))
		ax.grid()

		# initialize the line drawn for the animation
		self.line, = ax.plot([],[],'o-',lw=2)
		self.line.set_data([],[])

		# perform the animation
		self.anim1 = animation.FuncAnimation(self.fig, self.anim_one, numpy.arange(1, len(theta)), interval=1, blit=False, init_func=self.init, repeat=True)

		# creates second subplot to graph the user-selected variable
		# if statements determine which variable to plot
		ax2 = self.fig.add_subplot(122)
		if(self.var_selec.get() == "Position (Angular)"):
			self.wave = ax2.plot(time, theta[:,0])
		elif(self.var_selec.get() == "Velocity (Angular)"):
			self.wave = ax2.plot(time, theta[:,1])

		self.canvas.show()

	# contains the modifications that define the animation
	def anim_one(self,i):
		self.currentx = [0, self.xx[i]]
		self.currenty = [0, self.yy[i]]
		self.line.set_data(self.currentx, self.currenty)
		return self.line

	# necessary init function for the animation function
	def init(self):
		return self.line

	# performs the simulation and graphing for the two-link pendulum
	def run2(self):
		self.fig.clear()

		# set constant variables
		gravity = 9.81
		delta_t = 0.1

		# obtain user defined variables from the GUI
		initial_angle = float(self.iangle.get())
		length = self.slider_length.get()
		mass = self.slider_mass.get()
		time_f = float(self.time_f.get())
		friction_coeff = self.slider_friction.get()

		# calculate inertia
		inertia = mass * length**2 / 3.0

		# set initial conditions
		state = numpy.array([initial_angle, 0.0, initial_angle, 0,0])*pi/180

		# function calculates the first and second derivates for odeint() to use
		def calc_derivative(state, time):
			dydx = numpy.zeros_like(state)
			dydx[0] = state[1]
			del_ = state[2] - state[0]
			den1 = (mass+mass)*length - mass*length*cos(del_)*cos(del_)
			dydx[1] = (mass*length*state[1]*state[1]*sin(del_)*cos(del_) + mass*gravity*sin(state[2])*cos(del_) + mass*length*state[3]*state[3]*sin(del_) - (mass+mass)*gravity*sin(state[0]))/den1
			dydx[2] = state[3]
			den2 = den1
			dydx[3] = (-mass*length*state[3]*state[3]*sin(del_)*cos(del_) + (mass+mass)*gravity*sin(state[0])*cos(del_) - (mass+mass)*length*state[1]*state[1]*sin(del_)- (mass+mass)*gravity*sin(state[2]))/den2
			return dydx

		# creates time array that odeint() uses to integrate
		time = numpy.arange(0.0, time_f, delta_t)

		# use numerical integration to solve pendulum state equations
		theta = integrate.odeint(calc_derivative, state, time)

		# create arrays with coordinates of the link over time
		self.xx1 = length*sin(theta[:,0])
		self.yy1 = -length*cos(theta[:,0])
		self.xx2 = self.xx1 + length*sin(theta[:,2])
		self.yy2 = self.yy1 - length*cos(theta[:,2])

		# create the subplot for the animation
		ax = self.fig.add_subplot(121, autoscale_on=False, xlim=(-20,20), ylim=(-20,20))
		ax.grid()

		# initialize the line drawn for the animation
		self.line, = ax.plot([],[],'o-',lw=2)
		self.line.set_data([],[])

		# perform the animation
		self.anim1 = animation.FuncAnimation(self.fig, self.anim_two, numpy.arange(1, len(theta)), interval=1, blit=False, init_func=self.init2, repeat=True)

		# creates second subplot to graph the user-selected variable
		# if statements determine which variable to plot
		ax2 = self.fig.add_subplot(122)
		if(self.var_selec.get() == "Position (Angular)"):
			self.wave = ax2.plot(time, theta[:,0])
		elif(self.var_selec.get() == "Position 2 (Angular)"):
			self.wave = ax2.plot(time, theta[:,2])
		elif(self.var_selec.get() == "Velocity (Angular)"):
			self.wave = ax2.plot(time, theta[:,1])
		elif(self.var_selec.get() == "Velocity 2 (Angular)"):
			self.wave = ax2.plot(time, theta[:,3])

		self.canvas.show()

	# contains the modifications that define the animation
	def anim_two(self, i):
		self.currentx = [0, self.xx1[i], self.xx2[i]]
		self.currenty = [0, self.yy1[i], self.yy2[i]]
		self.line.set_data(self.currentx, self.currenty)
		return self.line

	# necessary init function for the animation function
	def init2(self):
		return self.line


# allows the user to select whether to run a single-pendulum or a double-pendulum simulation
simulation_toggle = input('Enter 1 for single-pendulum simulation and 2 for double-pendulum simulation: ')

if simulation_toggle != 1 and simulation_toggle != 2:
	print 'Please enter a valid choice.'
	sys.exit()

# runs the GUI
root = tk.Tk()
root.lift()
app = Interface(master=root)
app.mainloop()
root.destroy()