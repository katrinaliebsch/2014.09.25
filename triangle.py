# #this is the pseudocode for assignment 2 to generate 3 points randomly, rotate, and save 100 different instances of the same triangle class to generate an image

# Triangle Class
# 	Variables:
# 		1st Vertex(+ Y-Axis)
# 		2nd Vertex(3rd Quadrant)
# 		3rd Vertex(4th Quadrant)

# 	Initiation Function(self, 1st Vertex, 2nd Vertex, 3rd Vertex):
# 		Pass 3 variables to class

# 	Generate Random Vertices(self):
# 		First Vertex: [0,positive_random_variable()]	
# 		Second Vertex: [negative_random_variable(),negative_random_variable() ]
# 		Third Vertex: [positive_random_variable(), negative_random_varialbe()]
# 		Note: for random variable functions, define variables within a 256x256 pixel image
# 		Create array of vertices


# #main
# Generate Image:
# 	create 256x256 blank array
# 	initial image = Triangle()

# #function to populate inside points of triangle
# Create Triangle Image(blank, triangle)
# 	Find Triangle Boundaries(triangle)
# 		slope12 = (y1 - y2) / (x1 - x2)
# 		slope13 = (y3 - y1) / (x3 - x1)
# 		slope23 = (y3 - y2) / (x3 - x2)
# 		Array12 for line from vertex1 to vertex2 -> for x from x2 to x1 define y = y2 + slope12 * (x - x2)
# 		Array13 for line from vertex1 to vertex3 -> for x from x1 to x3 define y = y1 + slope13 * (x - x1)
# 		Array23 for line from vertex2 to vertex3 -> for x from x2 to x3 define y = y2 + slope23 * (x - x2)
# 		return three arrays

# 	For all x points in image
# 		For all y points in image
# 			If x is negative check if coordinate is in between lines array12 and array23
# 			If x is positive check if coordinate is in between lines array13 and array23
# 			If coordinate falls in between either set as bright pixel

#This python code requires openCV to be downloaded to run cv2.cornerHarris() and cv2.dilate()
import numpy as np
import matplotlib.pyplot as plt
import random
import matplotlib.animation as anim
from scipy import ndimage
from scipy import misc

import cv2



#create a class to define the triangle we are creating
class triangle:
	#set default triangle vertices or allow user to define them
	def __init__(self, v1 = [0,1], v2 = [-1,-1], v3 = [1,-1]):
		self.v1 = v1 #v1 is on positive y-axis
		self.v2 = v2 #v2 is in quadrant 3
		self.v3 = v3 #v3 is in quadrant 4

	def generate_rand_verts(self):
		self.v1 = [0, 128.0*random.random()]
		self.v2 = [-128.0*random.random(), -128.0*random.random()]
		self.v3 = [128.0*random.random(), -128.0*random.random()]


#create arrays saving the coordinates of the triangle edges
def find_triangle_bound(triangle_obj):
	#read vertex coordinates into individual variables
	x1, x2, x3 = triangle_obj.v1[0], triangle_obj.v2[0], triangle_obj.v3[0]
	y1, y2, y3 = triangle_obj.v1[1], triangle_obj.v2[1], triangle_obj.v3[1]

	#define slopes between vertices
	slope12 = (y1 - y2)/(x1 - x2)
	slope13 = (y3 - y1)/(x3 - x1)
	slope23 = (y3 - y2)/(x3 - x2)

	#define arrays with edge coordinates
	line12 = []
	line13 = []
	line23 = []

	for x in range(int(x2)+1, int(x1)+1):
		line12.append([x, y2 + slope12*(x - x2)])

	for x in range(int(x1), int(x3)+1):
		line13.append([x, y1 + slope13*(x - x1)])

	for x in range(int(x2)+1, int(x3)+1):
		line23.append([x, y2 + slope23*(x - x2)])

	return (line12, line13, line23)


def create_triangle_img(triangle_img, triangle_obj):
	line12, line13, line23 = find_triangle_bound(triangle_obj)

	for x in range(0,128):
		for y in range(0,255):
			for z in line12:
				if(x-127 == z[0]):
					if(127-y <= z[1]):
						for w in line23:
							if(x-127 == w[0]):
								if(127-y >= w[1]):
									triangle_img[y][x] = 1
	
	for x in range(128,255):
		for y in range(0,255):
			for z in line13:
				if(x-127 == z[0]):
					if(127-y <= z[1]):
						for w in line23:
							if(x-127 == w[0]):
								if(127-y >= w[1]):
									triangle_img[y][x] = 1


#this function rotates triangle and saves 100 different png files in specified directory
def rotandsave_img(triangle_img, ang_vel, frames=100):
	rot_angle = ang_vel * 0.033333


	#with writer.saving(fig, "writer_test.mp4", 100):
	for x in range(0,frames):
			triangle_img = ndimage.rotate(triangle_img, rot_angle, reshape=False)
			misc.imsave('triangle%02d.png' % x, triangle_img)




#create blank image
triangle_image = np.zeros((256,256))

#create the triangle and create random vertices
tri1 = triangle()
tri1.generate_rand_verts()
print "Vertex 1", tri1.v1
print "Vertex 2", tri1.v2
print "Vertex 3", tri1.v3
create_triangle_img(triangle_image, tri1)

#allow user to set gaussian filter
gauss_var = input('Enter a value for the gaussian blur setting: ')
triangle_image = ndimage.gaussian_filter(triangle_image, gauss_var)

ang_vel = input('Please specify an angular velocity in deg/s: ')
rotandsave_img(triangle_image, ang_vel)



#the following lines save our video of all png's
video=cv2.VideoWriter('movie.avi', -1, 30, (256,256))

for i in range(0,99):
	image = cv2.imread('triangle%02d.png' % i)
	video.write(image)

cv2.destroyAllWindows()
video.release()


harris_vertex_image = np.float32(triangle_image)

harris_vertex_image = cv2.cornerHarris(harris_vertex_image,5,3,0.04)
harris_vertex_image = cv2.dilate(harris_vertex_image,None)


#display harris vertex image
plt.imshow(harris_vertex_image, cmap=plt.cm.gray)
plt.title("Harrison Vertex Image")
plt.show()

gauss_weights = ndimage.gaussian_filter(harris_vertex_image, 2)
plt.imshow(gauss_weights, cmap=plt.cm.gray)
plt.title("Harrison Vertex Image with Gaussian Dialation")
plt.show()

#display image
plt.imshow(triangle_image, cmap=plt.cm.gray)
plt.title("Triangle Image Example")
plt.show()

# plt.imshow(triangle_image_rot, cmap=plt.cm.gray)
# plt.show()

